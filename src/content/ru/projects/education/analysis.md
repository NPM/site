---
content_type: project
project_type: education
id: analysis
shortTitle: Курс по анализу данных
title: Статистические методы в экспериментальной физике
courseName: stat-methods
order: 2
published: true
language: ru
---
Семестровый курс по выбору для студентов 2&ndash;4 курсов.