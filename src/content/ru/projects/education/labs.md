---
content_type: project
project_type: education
id: labs
shortTitle: Лабораторные работы
title: Лабораторные работы на кафедре общей физики
courseName: biref
order: 10
published: true
language: ru
---

Группа участвует в усовершенствовании лабораторных работ на кафедре общей физики МФТИ.

### Двулучепреломление

Создан программный пакет и дополнительное описание для обработки данных в лабораторной работе по двулучепреломлению в
рамках курса оптики (IV семестр общей физики).

Веб приложение для обработки доступно [тут](https://npm.mipt.ru/apps/biref).

Подробности на [странице проекта](/pages/biref).

Исходный код [в github](https://github.com/mipt-genphys/birefringence).

### Методическое пособие по проведению и обработке результатов учебного эксперимента

Пособие предназначено для студентов младших курсов и описывает проведение, обработку и представление результатов типичного учебного эксперимента (лабораторной работы).

**В данный момент пособие дорабатывается и будет обновляться.** 

[Версия в формате PDF для скачивания](/files/main.pdf).

[Электронная версия для чтения on-line](http://npm.mipt.ru/books/lab-intro/)