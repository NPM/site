---
content_type: project
project_type: physics
id: iaxo
shortTitle: IAXO
title: Международная коллаборация IAXO
order: 6
published: false
language: ru
---

**IAXO** (International Axion Observatory) &mdash; это новое поколение аксионного гелиоскопа, чья основная задача &mdash; обнаружение аксионов (или других элементарных частиц), в большом количестве излучаемых ядром Солнца. 

**Аксионы** &mdash; гипотетические частицы, предложенные в расширении стандартной модели физики частиц. Их существование не доказано экспериментально, но есть серьёзные теоретические основания подозревать это. Также они связаны с проблемой тёмной материи.

Для преобразования аксионов в фотоны гелиоскоп использует мощное магнитное поле. Используется тороидальный сверхпроводящий магнит длиной 20м с восемью катушками и восемью отверстиями диаметром 60 см, расположенными между катушками. Этот магнит будет помещен на движущуюся структуру, очень похожую на обычную телескопическую, чтобы направить магнит на Солнце. В конце отверстий магнита специально сконструированная рентгеновская оптика фокусирует предполагаемые аксионные фотоны в небольшие области (0,2см$^2$) на фокусном расстоянии около 5 метров. Каждое из фокусных пятен будет отображаться с помощью ультранизких фоновых рентгеновских детекторов Micromegas.

**Задача лаборатории** &mdash; разработка программного обеспечения и системы медленного контроля.

<img src="/images/projects/physics/iaxo.png" alt="IAXO"/>