---
content_type: project
project_type: physics
id: numass
shortTitle: Троицк ню-масс
title: Установка по поиску массы нейтрино Троицк ню-масс
order: 3
published: true
language: ru
---

<img src="/images/projects/physics/spectrometer900.jpg" alt="spectrometer"/>

Установка &laquo;Троицк ню-масс&raquo; является одним из немногих действующих в России экспериментов мирового уровня в области физики элементарных частиц. Цель эксперимента &mdash; поиск масс как активных, так и стерильных нейтрино. Результаты, полученные на установке, в настоящее время являются лучшими в мире.