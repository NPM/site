---
content_type: project
project_type: physics
id: mounMonitor
shortTitle: Мюонный монитор
title: Мюонный монитор для подземных низкофоновых экспериментов
order: 2
published: true
language: ru
---
**Расположение:** Испания, Канфранк-Эстасьон (Canfranc-Estación),
подземная лаборатория в Канфранке (LSC Laboratorio Subterráneo de
Canfranc)

<img src="/images/projects/physics/map.png" alt="map"/>

**Задача:** регистрация групп космических мюонов высоких энергий в
условиях подземной лаборатории LSC.

**Схема установки:** базовыми детектирующими элементами монитора
являются сцинтилляционные детекторы мюонов SC16, каждый из которых в
свою очередь состоит из 16 единичных сцинтилляционных детекторов
SC1 («пикселей») и внутренней электроники.

В состав системы сбора данных (DAQ) входят блоки обработки сигналов о
времени (TimeBoard) и координате (HodoscopeBoard) сработавших пикселей;
блоки низковольтного питания для детекторов; VME units и компьютер для
финального вывода исходных файлов; Trigger Unit для отбора мюонных
событий в реальном времени.

DAQ system:

Electronics

-   Power supply for the sc16 detectors

-   Hodoscope board (pattern info)

-   Time master board (timing info)

-   7 power supplies for these boards

-   VME modules

-   VME Pattern unit V1495

-   Computer with data acquisition program

-   Trigger Unit (real time selection)

Сцинтилляционные детекторы SC16 сцинтилляциой системы монитора
сгруппированы в три слоя. Верхний и нижние слои состоят из 9 детекторов
SC16 с общим количеством сцинтилляторов SC1 в каждом слое, равным 9\*16
= 144. Средний слой состоит из 4 детекторов SC16 т.&nbsp;е. 64 сцинтилляторов
SC1.

Детекторы среднего и верхнего слоев покоятся на деревянной раме. Рама
опирается на нижний слой. Сверху сборка из трех слоев накрыта свинцовым
экраном.

<img src="/images/projects/physics/setup.png" alt="setup"/>

Схема сборки детекторов