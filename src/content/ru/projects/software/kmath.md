---
content_type: project
project_type: software
id: kmath
shortTitle: KMath
title: "KMath - экспериментальная математическая библиотека на Kotlin"
order: 2
published: true
language: ru
---

Экспериментальная библиотека для математических операций на Kotlin, построенная по принципу контекстно-ориентированного
программирования с учетом математических абстракций.

[Репозиторий с кодом и документация](https://github.com/SciProgCentre/kmath).