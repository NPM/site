---
content_type: project
project_type: software
id: reactor
shortTitle: Моделирование RL-TGE
title: Макро-моделирование для реакторной модели в физике атмосферы
order: 10
published: true
language: ru
---

Макро-симуляция для реакторной модели в физике электронных лавин в атмосфере на языке Kotlin.

[Репозиторий с кодом](https://bitbucket.org/mipt-npm/skysim).
