---
content_type: project
project_type: software
id: muon-sim
shortTitle: Моделирование Muon Monitor
title: Модель для анализа данных эксперимента Muon Monitor
order: 9
published: true
language: ru
---

Моделирование и графическая визуализация регистрации мюонов в эксперименте Muon Monitor написанные на языке Kotlin.

[Репозиторий с кодом и инструкции по запуску](https://bitbucket.org/mipt-npm/muon-sim).
