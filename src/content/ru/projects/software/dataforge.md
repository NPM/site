---
content_type: project
project_type: software
id: dataforge
shortTitle: DataForge
title: Система автоматизированной обработки данных DataForge
order: 1
published: true
language: ru
---

[DataForge](/dataforge) &mdash; это современная платформа для сбора и анализа данных, созданная для автоматизации обработки данных в физических экспериментах и не только.