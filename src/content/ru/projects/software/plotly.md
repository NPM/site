---
content_type: project
project_type: software
id: plotly
shortTitle: Plotly.kt
title: Plotly.kt для Kotlin Multiplatform
order: 5
published: true
language: ru
------------

[Plotly.kt](https://github.com/mipt-npm/plotly.kt) построена на основе популярной веб-библиотеки [Plotly](https://plotly.com/javascript/). Она позволяет осуществлять доступ к почти всем функциям этой библиотеки из Kotlin Multiplatform (Kotlin/JVM и Kotlin/JS). Также есть экспериментальная интеграция с Kotlin kernel для IPython/Jupyter и другие способы отрисовки графиков.

Более подробное описание &mdash; в [репозитории проекта](https://github.com/mipt-npm/plotly.kt), а также [на специальной странице с примерами](/files/plotly.html), подготовленной стажеркой JBR Екатериной Самородовой.