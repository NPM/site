---
content_type: member
title: Егор Стадничук
id: stadnichuk
photo: stadnichuk.jpg
order: 60
published: true
language: ru
---
Научный сотрудник лаборатории. Аспирант ВШЭ.

Руководитель работ по сегментированному спутниковому детектору. Основной участник работ по атмосферной физике.