---
content_type: member
title: Федор Букреев
id: bukreev
order: 510
photo: bukreev.jpg
published: false
language: ru
---
Студент МФТИ. Преподаватель информатики в МФТИ.

Входит в группу ПО. Занимается системами распределенных вычислений.