---
content_type: member
title: Александр Светличный
id: svetlichnii
order: 14
photo: svetlichny.jpeg
published: true
language: ru
---

Стажёр-исследователь ИЯИ РАН, аспирант и научный сотрудник МФТИ, преподаватель кафедры общей физики, заместитель руководителя [магистратуры по научному программированию](/magprog).

Занимается Монте-Карло моделированием релятивистских ядро-ядерных столкновений под руководством [И. А. Пшеничнова](https://ru.wikipedia.org/wiki/%D0%9F%D1%88%D0%B5%D0%BD%D0%B8%D1%87%D0%BD%D0%BE%D0%B2,_%D0%98%D0%B3%D0%BE%D1%80%D1%8C_%D0%90%D0%BD%D0%B0%D1%82%D0%BE%D0%BB%D1%8C%D0%B5%D0%B2%D0%B8%D1%87).  Также, занимается развитием современного образования и новыми подходами к образованию.

[ORCID](https://orcid.org/0000-0002-2086-7045)

[ResearchGate](https://www.researchgate.net/profile/Alexandr-Svetlichnyy)
