---
content_type: member
title: Петр Климай
id: klimai
order: 2
photo: klimai.jpg
published: true
language: ru
---
**Заместитель заведующего лабораторией**

Кандидат физико-математических наук. 

Руководитель работ лаборатории по ПО для эксперимента BM@N.

Список основных публикаций в базе [INSPIRE](https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=klimai&ui-citation-summary=true)
