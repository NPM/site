---
content_type: member
title: Андрей Выборов
id: vyborov
order: 21
photo: vyborov.png
published: true
language: ru
---
Аспирант МФТИ. Преподаватель общей физики в МФТИ.

Руководитель рабочей группы по фону от солнечных нейтрино.

