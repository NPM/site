---
content_type: member
title: Тимур Хамитов
id: khamitov
order: 15
photo: khamitov.jpg
published: true
language: ru
---
Аспирант МФТИ.

Занимается моделированием процессов в детекторах разного назначения.
Входит в группу моделирования и участвует в исследовании атмосферных разрядов.
