---
content_type: member
title: Михаил Зеленый
id: zelenyy
order: 17
photo: zelenyy.jpg
published: true
language: ru
---
Аспирант ИЯИ РАН. Преподаватель МФТИ.

Руководитель рабочей группы по моделированию физических процессов.