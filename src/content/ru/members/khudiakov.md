---
content_type: member
title: Алексей Худяков
id: khudyakov
order: 12
photo: khudyakov.jpg
published: false
language: ru
---
Научный сотрудник ИЯИ РАН.

Специалист по редким распадам каонов, системам сбора данных и функциональному программированию.
