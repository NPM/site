---
content_type: member
title: Лев Владиславович Инжечик
id: inzhechik
order: 1
page_link: /about/inzhechik
photo: Inzhechik.jpg
published: true
language: ru

---

**Заведующий лабораторией**

Профессор кафедры общей физики МФТИ. Лауреат премии им. И.В. Курчатова. Ветеран атомной энергетики и промышленности. Официальный эксперт Росатома по изотопным технологиям. Участник международных физических экспериментов:  
[GERDA &mdash; search for neutrinoless double beta decay of Ge-76](https://www.mpi-hd.mpg.de/gerda/home.html)  
[EMMA &mdash; experiment with multimuon array](http://www.cupp.fi/index.php?option=com_content&view=article&id=4&Itemid=40&lang=en)  
Mu-Monitor &mdash; investigation of cosmic muon fluxes in underground labs LSC   
[CUPP](http://www.cupp.fi/)

**Научные интересы:** нейтринная физика и методика подземных низкофоновых экспериментов по физике ядра и частиц.

Более 100 научных публикаций, в числе которых:
* [P.Kuusiniemi et al. Muon multiplicities measured using an underground cosmic-ray array. J.Phys.Conf.Ser. 718 (2016) no.5, 052021.](https://inspirehep.net/record/1468840/files/JPCS_718_5_052021.pdf)  
* [M.Agostini et al. The background in the 0νββy experiment GERDA. Eur.Phys.J. C 74 (2014) 2764](http://arxiv.org/pdf/1306.5084.pdf)  
* [T.Raiha¤,et al. Cosmic-ray experiment EMMA: Tracking analysis of the first muon events. Proceedings of the 31st ICRC, Łodz 2009;]( http://icrc2009.uni.lodz.pl/proc/pdf/icrc0996.pdf)  
* [M.Agostini et al. Measurement of the half-life of the two-neutrino double beta decay of Ge-76 with the Gerda experiment. J. Phys. Nucl. Part. Phys. 40 (2013) 035110;](http://arxiv.org/pdf/1212.3210.pdf)  
* [M.Agostini et al. Results on Neutrinoless Double-βDecay of 76Gefrom Phase I of the GERDA Experiment. Phys. Rev. Lett. 111 (2013) 122503;]( http://arxiv.org/pdf/1307.4720.pdf)  
* [J.Sarkamo et al. EAS selection in the EMMA underground array. J. Phys.: Conf. Ser.(2013)409 012086;](http://iopscience.iop.org/article/10.1088/1742-6596/409/1/012086/pdf)  
* Л.В. Инжечик. Изотопы. Изотопов разделение. Большая Российская Энциклопедия. Моск. научн. изд. «Большая Российская Энциклопедия», 2008. Т. 11, с. 33, 34.  
* Ю.В. Гапонов, Л.В. Инжечик, С.В. Семенов. Изотопы и фундаментальные проблемы ядерной физики. Глава 10 коллективной монографии «Изотопы: свойства, получение, применение» под ред. В.Ю. Баранова, М, Физматлит, 2005, т. 2.

e-mail: [inzhechik@mail.ru](inzhechik@mail.ru)
