---
content_type: member
title: Эля Блинова
id: blinova
order: 550
photo: Elya.jpg
published: false
language: ru
---
Студентка МФТИ.

Входит в группу ПО. Занимается поддержкой сайта и системами ввода-вывода.