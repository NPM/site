---
content_type: member
title: Татьяна Абрамова
id: abramova
order: 70
photo: abramova.jpg
published: false
language: ru
---
Студентка ФКН ВШЭ.

Участвует в проектах по разработке программного обеспечения и в коллаборации BAT.
