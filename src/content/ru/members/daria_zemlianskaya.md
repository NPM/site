---
content_type: member
title: Дарья Землянская
id: zemlianskaya
order: 520
photo: zemlianskaya.jpg
published: false
language: ru
---

Студентка МФТИ, группа атмосферной физики, занимается моделированием процессов в атмосферной физике.