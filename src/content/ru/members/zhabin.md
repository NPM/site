---
content_type: member
title: Сергей Николаевич Жабин
id: zhabin
order: 11
photo: zhabin.jpg
published: false
language: ru
---
Кандидат физико-математических наук. Преподаватель МФТИ.