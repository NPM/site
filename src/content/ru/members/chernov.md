---
content_type: member
title: Василий Чернов
id: chernov
order: 22
photo: chernov.png
published: false
language: ru
---
Кандидат физико-математических наук.

Специалист по системам сбора данных. Участвует в экспериментах Troitsk nu-mass и IAXO.

