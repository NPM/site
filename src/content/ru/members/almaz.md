---
content_type: member
title: Алмаз Фазлиахметов
id: almaz
order: 20
photo: almaz.jpg
published: true
language: ru
---
Аспирант МФТИ.

Руководитель рабочей группы по [мюонному монитору](/projects/physics#mounMonitor).

