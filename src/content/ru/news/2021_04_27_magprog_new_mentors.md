---
content_type: post
title: Новые менторы магистерской программы
date: 2021-04-27
slug: /magprog
language: ru
---

Добавлены два новых руководителя [магистерской программы](/magprog):

* Айно Скасырская (ИЯИ РАН): ведущий специалист по научными и инженерным сеточным расчетам в ANSIS и COMSOL.
* Дмитрий Костюнин (DESY, JetBrains Research): специалист по анализу данных астрофизических наблюдений.
