---
content_type: post
title: Статистические методы — 2021
date: 2021-09-01
slug: /statmethods-2021
language: ru
---

Запускаем курс по [статистическим методам](http://npm.mipt.ru/ru/pages/stat-methods) в 2021 году

Курс ориентирован на практику применения с использованием языков программирования Python и Kotlin.
Курс планируется по четвергам, начиная с 9 сентября, в 15:30 в Физтех.Цифре 5.16, формат гибридный: занятия в аудитории плюс онлайн-трансляция. Ссылка на трансляцию будет доступна в телеграм-чате.

Телеграм-чат курса: [@mipt_statmethods](https://t.me/mipt_statmethods).
