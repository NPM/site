---
content_type: post
title: Семинар по программированию
date: 2019-06-10
published: true
slug: /prog_seminar
language: ru
---
Лаборатория совместно с ОИЯИ и при поддержке [JetBrains Research](https://research.jetbrains.org/groups/npm) 
запускает серию семинаров по программированию в
экспериментальной физике (и не только). Информация о семинаре доступна [тут](projects/../../../projects/software#prog-seminar)
и [тут](http://npm.mipt.ru/confluence/pages/viewpage.action?pageId=33128452). Если вы хотите участвовать, то пожалуйста помогите определиться
с датами первого семинара ( [doodle](https://doodle.com/poll/gircdvu5kya77twf) ). Также, если у вас есть что рассказать, пишите
на почту или в телеграм. 