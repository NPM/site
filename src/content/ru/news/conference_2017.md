---
content_type: post
title: Молодежная конференция 2017
date: 2017-01-31
published: true
slug: /conf_2017
language: ru
---

<img className="img-fluid" src="/images/index/conference_2017_header.png" className="img-responsive" alt="conference slider">

Открыта регистрация на III молодежную конференцию по физике элементарных частиц и космологии. Конференция пройдет 27 и 28 апреля. 

Регистрация и дополнительная информация доступны на [сайте конференции](http://npm.mipt.ru/conf).
 
Листовка с информацией доступна [здесь](/files/conference_2017_invitation.pdf).