---
content_type: post
title: Научное программирование на Kotlin — 2020
date: 2020-02-07
published: true
slug: /kotlin_2020
language: ru
---

Открыта регистрация на курс &laquo;Введение в научное программирование на языке Kotlin&raquo;. В этом году курс проходит при официальном участии JetBrains и поддержке [JetBrains Research](https://research.jetbrains.org/groups/npm).

Страница курса доступна [тут](/pages/kotlin). Для участия в курсе следует зарегистрироваться [тут](https://docs.google.com/forms/d/e/1FAIpQLSeNZT8B90pT6fM9oABHFbrtv6pKfoYKfO-ANAjLlgWynMnh_g/viewform).