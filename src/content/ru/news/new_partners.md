---
content_type: post
title: Новые партнеры
date: 2016-11-21
published: true
slug: /partners
language: ru
---
19 ноября прошла очередная рабочая встреча группы. В рамках встречи в частности обсуждали два новых соглашения о сотрудничестве.

Начато сотрудничество с компанией МТЛ. В рамках этого сотрудничества планируется совместное исследование и разработка рентгеновских панелей, а также подготовка студентов.

Также в рамках сотрудничества с ИКИ РАН создана рабочая группа по моделированию электрических разрядов в верхних слоях атмосферы.

Обновлен раздел ["Партнеры"](/partners)