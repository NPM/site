---
content_type: post
title: Обновление 20.01.2017
date: 2017-01-20
published: true
slug: /update
language: ru
---
<p>Обновлены некоторые разделы сайта. Создан <a href="http://npm.mipt.ru/confluence">внутренний раздел</a>.</p>