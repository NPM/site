---
content_type: page_education
title: Birefringence laboratory work
shortTitle: Birefringence laboratory work
parent: education
path: /pages/biref
slug: /en/pages/biref
published: true
language: en
---
# Application
Application is available at [https://npm.mipt.ru/apps/biref](https://npm.mipt.ru/apps/biref). It is being run in a browser, so it does require installation.

# Additional theoretical background
Additional materials on the mathematical and physical substantiation of the work of analysis programs can be found <a href="/files/biref.pdf">here</a>.

# Additional task
## 1. Data entry
For further work, you must enter data in the table. This can be done manually or by loading text data from a file.

## 2. Checking the error
In the work, there are practically no parameters that can have a significant systematic bias, and the main error comes from the inaccuracy of measuring angles. Moreover, the value of this error is set from naive considerations, since the measuring dial in this case does not have any particular accuracy class.

You can verify the accuracy of the error determination according to the graph of the refractive index of an ordinary wave. From theoretical considerations, it is known that the points of this graph should lie on a straight line with zero slope (constant). The scatter of points relative to this line should be purely statistical in nature. If the error values ​​are significantly less than the average point spread relative to the straight line, then the errors are underestimated. If the error values ​​are greater than the spread of points, then the errors are overestimated. A more accurate characterization of the magnitude of errors can be obtained using <a href="https://ru.wikipedia.org/wiki/%D0%9A%D1%80%D0%B8%D1%82%D0%B5%D1%80%D0%B8%D0%B9_%D1%81%D0%BE%D0%B3%D0%BB%D0%B0%D1%81%D0%B8%D1%8F_%D0%9F%D0%B8%D1%80%D1%81%D0%BE%D0%BD%D0%B0">Pearson's consent criterion</a> (it is also $\chi^2$ ). According to this criterion, the value of the sum $\chi^2 = \sum{\frac{(y_i-f(x_i))^2}{\sigma_i^2}}$, relative to the number of degrees of freedom (usually this is the number of points minus the number of free parameters) for a sample that obeys statistical laws, it should be close to <code> 1 </code>. In this case, you can use the function <code> Check calibration </code> in the program. As a result of this function, two $\chi^2$ values ​​are returned: one for comparison with a linear dependence, the second for comparison with a constant that follows from the theory. In the first case, the number of degrees of freedom is one less, since a linear dependence requires two parameters instead of one. You can use both dependencies to check for errors.

**Important:** It should be noted that in experimental physics, arbitrary selection of errors is generally prohibited. The determination of errors occurs before the start of the analysis and cannot be based on the results of measurements. The “fitting” of errors is allowed only if there are no physical considerations regarding the included errors, and also when the absence of systematic biases is guaranteed.

**Note:** The incorrect determination of errors in this work usually occurs due to an incorrect assessment of the accuracy of measurements on a scale. As a rule, they take half the scale division for such an assessment. In fact, even if all measured values are rounded towards the nearest integer (which is not recommended), the deviation of the true value from the measured value is described <a href="https://ru.wikipedia.org/wiki/%D0%9D%D0%B5%D0%BF%D1%80%D0%B5%D1%80%D1%8B%D0%B2%D0%BD%D0%BE%D0%B5_%D1%80%D0%B0%D0%B2%D0%BD%D0%BE%D0%BC%D0%B5%D1%80%D0%BD%D0%BE%D0%B5_%D1%80%D0%B0%D1%81%D0%BF%D1%80%D0%B5%D0%B4%D0%B5%D0%BB%D0%B5%D0%BD%D0%B8%D0%B5">even distribution </a> with a width of one degree. The standard deviation of such a distribution is $\frac{1}{\sqrt{12}}$, not <code>0.5</code>.

## 3. Statistical determination of the angle at the apex of the prism
In the main description of this work, an experimental determination of the angle at the apex of the prism is given. But this angle can also be determined on the basis of the measured data. To do this, it suffices to postulate that the dependence of the refractive index, measured by an ordinary wave, has a zero slope.

By varying the <code> A </code> parameter, you can select a value at which, after calibration, the line corresponding to a fixed constant on the graph is combined with the line that corresponds to a linear relationship. In addition, you can use the statistical properties of the dependencies and find a <code> A </code> value such that the data matches the constant will be the best ($\chi^2$ with respect to the weighted average).

Also in this way you can get the measurement error <code> A </code>. The value of $\chi^2$ is inversely proportional to the logarithm <a href="https://ru.wikipedia.org/wiki/%D0%A4%D1%83%D0%BD%D0%BA%D1%86%D0%B8%D1%8F_%D0%BF%D1%80%D0%B0%D0%B2%D0%B4%D0%BE%D0%BF%D0%BE%D0%B4%D0%BE%D0%B1%D0%B8%D1%8F">of likelihood function</a>, which usually (not always) has the form of a normal distribution. As a result, the graph $\chi^2(A)$ has the form of a parabola. If in this graph we postpone from the minimum value on the vertical axis <code> 1 </code> up and project this point onto the horizontal axis (we get one point to the right and one to the left), then the resulting interval will just correspond to 1 - $\sigma$ interval for the normal distribution, that is, just what is usually used to determine the errors.

**Important:** The <code> A </code> coefficient so determined is not necessarily a true physical value. It is only the most likely for a given dataset and the zero slope hypothesis. To be sure of the results, it is necessary to compare the angle obtained in the experiment and from the statistical procedure. If the experimental value does not fall into the $2\sigma$ interval relative to the statistical one, this is an occasion to think about whether the measurements were made correctly.

## 4. Determination of the correlation of the angle ** <code> A </code> ** and the refractive indices
Having the angle error <code> A </code>, you can get the systematic error of the resulting values ​​$n_o$ and $n_e$. The usual calculation of the correlation coefficient through derivatives can be quite difficult, therefore, the coefficient can be determined “experimentally”. To do this, it is enough to plot the displacement graphs of $n_o$ and $n_e$ relative to <code> A </code> in the vicinity of the most probable value. The slope coefficient of this graph will show the relationship between the systematic error $n_o$ or $n_e$ and the error <code> A </code>.

**Note:** This method also allows you to check the linearity of the dependence of the parameter displacement.