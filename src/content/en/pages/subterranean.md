﻿---
content_type: course
title: Low-background experiments in physics of the nucleus, particles and astrophysics
shortTitle: Low-background experiments
parent: education
path: /pages/subterranean
slug: /en/pages/subterranean
published: true
language: en
---
<p className="lead text-center">
OPTIONAL SEMESTER COURSE
</p>

[2018 announcement](/files/npm-2018.pdf)

The lecture course analyzes the problems of modern neutrino physics: rest mass, oscillations, non-conservation of lepton number, sterile neutrinos. A review of experimental studies of neutrinos of various origin (reactor, solar, atmospheric, accelerator, galactic and geoneutrino) in underground, underwater and under-ice low-background laboratories of the world is given. Methods of searching and studying other rare nuclear-physical and astrophysical phenomena that are not described by the standard model are considered.

We will talk about current results, as well as plans for the development of the international GERDA experiment (search for neutrinoless double beta decay of Ge-76, which violates the lepton number), about the Troitsk nu-mass and KATRIN experiments (precision measurement of the tritium beta decay spectrum for determination of the neutrino resting mass), EMMA, and Mu-monitor (measuring the fluxes of high-energy cosmic muons penetrating underground). These studies are conducted in the underground laboratories of Italy (LNGS), Finland (CUPP) and Spain (LSC), at the research centers of the INR RAS (Troitsk) and the University of Mainz.
 
The lecturer is a participant of the international collaborations GERDA and EMMA, the leader of the Mu-monitor project, the head of the MIPT Nuclear Physics Experiment Methods group. Students - members of the group - conduct scientific and educational work in international pilot projects, participate in meetings and directly cooperate with researchers from different countries.


<figure className="figure">
    <img  src="/images/projects/physics/GERDA.jpg" className="figure-img img-fluid" alt="GERDA">
    <figcaption className="figure-caption">
        GERDA experiment. Installation of cryostat for 60 m3 of liquid argon. Underground experimental hall LNGS in the thickness of the Gran Sasso Ridge, Italy, 2011.
    </figcaption>
</figure>


# Course program

1. Basic concepts and terminology of physics of fundamental particles and interactions.

2. Physical problems of low-background underground experiments: neutrino flux registration, measurement of neutrino oscillations, double beta decay, search of "dark matter", study of high energy cosmic muons (EMMA project), prospects of multi-purpose underground low-background mega detectors development.

3. Experimental methods of particle detection, technology of low-background underground experiments. Sources of background radiation in underground measurements and methods of their suppression.

4. Neutrino detection methods, pioneering experiments of Davis and Raines.

5. Modern experimental physics of neutrinos. Detection of neutrinos, neutrino isotope sources.

6. The problem of injecting neutrino masses. Dark matter and beyond the standard model.

7. Direct determination of neutrino resting mass by precision measurement of the spectrum of tritium beta decay electrons: "Troitsk nu-mass" and KATRIN experiments.

8. Double beta decay of nuclei - experimental study of two neutrinos and search for neutrinos-free modes, international experiments Heidelberg-Moscow and GERDA, project LEGEND.


<figure className="figure">
    <img  src="/images/projects/physics/spectrometer900.jpg" className="figure-img img-fluid" alt="NUMASS">
    <figcaption className="figure-caption">
        Troitsk nu-mass experiment. One of the largest ultra-high vacuum volumes in the world. Troitsk, Moscow. 2018.
    </figcaption>
</figure>

<figure className="figure">
    <img  src="/images/projects/physics/katrin.jpg" className="figure-img img-fluid" alt="KATRIN">
    <figcaption className="figure-caption">
        KATRIN experimant. The next generation of neutrino mass search experiments. Karlsruhe, Germany.
    </figcaption>
</figure>
