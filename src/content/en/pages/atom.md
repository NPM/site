---
content_type: course
title: History of the atomic project
shortTitle: History of the atomic project
parent: education
path: /pages/atom
slug: /en/pages/atom
published: true
language: en
---
<p className="text-center">Episodes of the history of the early stages of the USSR atomic project </p>

<p className="lead text-center">
OPTIONAL SEMESTER COURSE
</p>

L.V. Inzhechik lectures on Mondays at 17:05 in the aud.517 MB. First lecture - September 18, 2017

The author of the course was one of the organizers of international conferences on the history of nuclear projects: ISAP-96 (Dubna) and ISAP-99 (Laxenburg, Austria). Historical information will be presented on the basis of the materials of these conferences, some of which have never gone out of print. Other publications on this subject and the memoirs of academician I.K. Kikoin [^1], professor of the Academy of Foreign Intelligence VB Barkovsky [^2] and other direct participants in the creation of the atomic bomb in the USSR, with whom the lecturer had a chance to talk.
A short and popular overview of the physical principles of nuclear energy will be given at the beginning of the course, so that students can relate historical facts to the scientific and technological aspects of the nuclear project. Some problems of peaceful nuclear energy will be considered and the causes and scenarios of the Kyshtym and Chernobyl nuclear disasters in the USSR will be analyzed, comments will be given on the accident at the Fukushima nuclear power plant in Japan.

[^1].	[http://www.biblioatom.ru/founders/kikoin_isaak_konstantinovich](http://www.biblioatom.ru/founders/kikoin_isaak_konstantinovich)

[^2].	[http://svr.gov.ru/history/bar.html](http://svr.gov.ru/history/bar.html)

<img src="/images/projects/physics/GERDA.jpg" alt="GERDA" />
