---
content_type: quarks
title: QUARKS-2020 conferention
shortTitle: QUARKS-2020
parent: physics
published: true
language: en
---
<img src = "/images/pages/Site_thumbnail8.png">  <!--папка /static-->

# International “QUARKS-2020” Workshop

From **7th to 13th of June 2020** in the city of Pereslavl-Zalessky the 21st International Seminar on High Energy Physics “Quarks-2020” will be held. The workshop will be organized with the support of [INR RAS](http://www.inr.ac.ru/), [JINR](http://www.jinr.ru/) and [MIPT](https://mipt.ru/).

The Workshop program includes plenary sessions, a poster session, and breakout discussions on the following topics:

* physics beyond the Standard Model;
* neutrino physics;
* quantum chromodynamics and strong interactions;
* heavy ion collisions;
* cosmology and particle astrophysics;
* gravity and its modifications;
* aspects of mathematical physics;
* some experimental results.

In addition, a number of [thematic meetings](https://indico.quarks.ru/event/2020/page/41-thematic-sections) are planned for this year.

Additional information available at Workshop site [quarks.ru](http://quarks.ru). To participate ypu should [register](https://indico.quarks.ru/event/2020/registrations/). Questions can be asked by mail [quarks@ms2.inr.ac.ru](mailto:quarks@ms2.inr.ac.ru).

There is no registration fee for MIPT students (accommodation must be paid). There is a possibility of limited financial support for active students (contact by mail indicated in the [contacts](/about) section.

<img src="/images/pages/Poster_QUARKS_2020.jpeg"/>