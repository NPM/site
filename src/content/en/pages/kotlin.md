---
content_type: course
title: Introduction to Kotlin scientific programming
shortTitle: Scientific programming
parent: education
path: /pages/kotlin
slug: /en/pages/kotlin
published: true
language: en
---

Recording of lectures in 2019 is available [here](https://www.youtube.com/playlist?list=PL4_hYwCyhAvZzRpbK4iTy9S6_OWZNEiVk).

## Course purpose

As physics (and science in general) develops, computer methods are becoming more and more important in the daily work of a scientist. In conducting an experiment, computer methods and tools are used at all stages of the work: planning the experiment, preparing the installation, collecting data, processing and publishing it. In such a situation, the quality of the programs used is beginning to play an important role. In addition, there is a need for specialists who understand both science and programming and who develop and improve software tools. 

Most students (and scientists) are more or less familiar with the basic tools of a programmer, for example, writing simple programs in Python. This is not enough for serious scientific development, so the course aims at a more advanced understanding of hardware, program structure and modern development tools.

As the main programming language we will use `Kotlin`, which appeared recently and managed to gain a large market share. Kotlin has several significant advantages as an initial language for advanced scientific programming:

* Strict typing, a clearly constructed system of types.
* High performance.
* Automatic memory management.
* Fully compatible with a huge number of Java libraries.
* Better toolkit.
* Extensive community.
* Possibility of commercial use.

## Lecturer

[Alexander Nozik](https://www.researchgate.net/profile/Alexander_Nozik) - experimental physicist, data analysis specialist in physical experiment and scientific software. 
Senior researcher at the INR RAS, Deputy Head of the MIPT LNPM. [JetBrains Research](https://research.jetbrains.org/groups/npm/) team leader.

[Andrey Shcheglov](https://www.linkedin.com/in/andreyshcheglov/?locale=en_US) - Senior Software Engineer at JetBrains.

## Course format

In 2020, the course is held with the participation of JetBrains and the support of JetBrains Research. The most active students will have the opportunity to participate in summer internships at JetBrains. There is also an opportunity for senior students to do research at the MIPT Laboratory of Nuclear Physics Experiments Methods (participant of JetBrains Research) and at the JetBrains Moscow office.

In this course we will learn to work in Kotlin language and apply it to scientific problems. We will focus on practical aspects and examples, so that no additional knowledge is needed to understand it. For practical examples we will use the development environment [IntelliJ IDEA Community Edition](https://www.jetbrains.com/idea/).

All questions related to the course will be discussed in telegram groups [@mipt-npm](https://t.me/mipt_npm) (scientific) and [Kotlin at MIPT](https://t.me/joinchat/EpV1201A_i0rTOCxHHnxXQ) (any questions about Kotlin).

All those wishing to participate should complete the [form](https://docs.google.com/forms/d/e/1FAIpQLSeNZT8B90pT6fM9oABHFbrtv6pKfoYKfO-ANAjLlgWynMnh_g/viewform).

## Course content

1.  **From hard to soft**
    1.  Program as a set of instructions. Evolution of programs.
    2.  Memory structure. Segmentation fault.
    3.  Programming paradigms. Genealogy of languages.
    4.  Virtual machines, byte-code.
    5.  Compilation and optimization.
    6.  Static and dynamic linking. Libraries.
    7.  Program structure. Entry points.
2.  **The tools of the modern programmer**
    1.  Automatic assembly systems.
    2.  Version control systems.
    3.  Integrated development environments.
3.  **Kotlin language**
    1.  Variables, classes and objects.
    2.  Control flow. Procedural and functional approach.
    3.  Short circuits.
    4.  Data structures and operations on them.
    5.  Properties and Delegates.
    6.  Parametric types.
    7.  Extensions.
    8.  Boxing.
    9.  Multiplatform projects
4.  **Program Architecture**
    1. Abstractions and interfaces.
    2. basics of collective development with the help of modern tools.
    3. Ideology of object programming. Separation of behavior.
    4. Ideology of functional programming.
5.  **Scientific programming**.
    1. Basics of numerical methods. The concept of numerical accuracy. Complexity of algorithms.
    2.  Numerical differentiation and integration.
    3.  Random Number Generators and Monte Carlo Modeling.
    4.  High-performance parallel and competitive computing.
    5.  The problem of I/O and the main methods to solve it.
    6.  Data collection systems. Protocols of data exchange.
    7.  Basics of work with big data.
    8.  Streaming data processing.

## Reporting

The offset is held in the form of a presentation based on the materials of an individual project. Interim reporting in the form of assignments is also provided.

## Recommended literature

* [Official documentation(en)](https://kotlinlang.org/docs/reference/)
* [Official documentation(ru)](https://kotlinlang.ru/)
* [Kotlin in action](https://dmkpress.com/catalog/computer/programming/java/978-5-97060-497-7/)
