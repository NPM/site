---
content_type: member
title: Fedor Bukreev
id: bukreev
order: 510
photo: bukreev.jpg
published: false
language: en
---
MIPT student. Teacher of computer science at MIPT.

Part of the software group. Works on distributed computing systems.
