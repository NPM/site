---
content_type: member
title: Elya Blinova
id: blinova
order: 550
photo: Elya.jpg
published: false
language: en
---
MIPT student.

Part of the software group. Engaged in site support and input-output systems development.
