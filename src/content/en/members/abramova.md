---
content_type: member
title: Tatyana Abramova
id: abramova
order: 70
photo: abramova.jpg
published: false
language: en
---
Student of HSE.

Participates in software development projects and in BAT collaboration.