---
content_type: member
title: Lev Vladislavovich Inzhechik
id: inzhechik
order: 1
photo: Inzhechik.jpg
published: true
language: en
---

**Head of the laboratory**

Professor, Department of General Physics, MIPT. Laureate of I. V. Kurchatov prize. Veteran of nuclear energy and industry. The official expert of Rosatom on isotope technologies. Member of international physical experiments: 
[GERDA &mdash; search for neutrinoless double beta decay of Ge-76](https://www.mpi-hd.mpg.de/gerda/home.html)  
[EMMA &mdash; experiment with multimuon array](http://www.cupp.fi/index.php?option=com_content&view=article&id=4&Itemid=40&lang=en)  
Mu-Monitor &mdash; investigation of cosmic muon fluxes in underground labs LSC   
[CUPP](http://www.cupp.fi/)

**Scientific interests:** neutrino physics and methodology for underground low-background experiments in nuclear and particle physics.

More than 100 scientific publications, including:
* [P.Kuusiniemi et al. Muon multiplicities measured using an underground cosmic-ray array. J.Phys.Conf.Ser. 718 (2016) no.5, 052021.](https://inspirehep.net/record/1468840/files/JPCS_718_5_052021.pdf)  
* [M.Agostini et al. The background in the 0νββy experiment GERDA. Eur.Phys.J. C 74 (2014) 2764](http://arxiv.org/pdf/1306.5084.pdf)  
* [T.Raiha¤,et al. Cosmic-ray experiment EMMA: Tracking analysis of the first muon events. Proceedings of the 31st ICRC, Łodz 2009;]( http://icrc2009.uni.lodz.pl/proc/pdf/icrc0996.pdf)  
* [M.Agostini et al. Measurement of the half-life of the two-neutrino double beta decay of Ge-76 with the Gerda experiment. J. Phys. Nucl. Part. Phys. 40 (2013) 035110;](http://arxiv.org/pdf/1212.3210.pdf)  
* [M.Agostini et al. Results on Neutrinoless Double-βDecay of 76Gefrom Phase I of the GERDA Experiment. Phys. Rev. Lett. 111 (2013) 122503;]( http://arxiv.org/pdf/1307.4720.pdf)  
* [J.Sarkamo et al. EAS selection in the EMMA underground array. J. Phys.: Conf. Ser.(2013)409 012086;](http://iopscience.iop.org/article/10.1088/1742-6596/409/1/012086/pdf)  
* L.V. Inzhechik. Isotopes. Isotope separation. Big Russian Encyclopedia. Moscow scientific ed. “Big Russian Encyclopedia”, 2008. V. 11, p. 33, 34.
* Yu.V. Gaponov, L.V. Inzhechik, S.V. Semenov. Isotopes and fundamental problems of nuclear physics. Chapter 10 of the collective monograph "Isotopes: Properties, Preparation, Application", ed. V.Yu. Baranov, Moscow, Fizmatlit, 2005, v. 2.

e-mail: [inzhechik@mail.ru](inzhechik@mail.ru)
