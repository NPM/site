---
content_type: member
title: Andrey Vyborov
id: vyborov
order: 21
photo: vyborov.png
published: true
language: en
---
MIPT postgraduate student. Teacher in General Physics at MIPT.

Head of the solar neutrino background working group.