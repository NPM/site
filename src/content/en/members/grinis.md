---
content_type: member
title: Roland Grinis
id: grinis
order: 12
photo: Grinis.jpg
published: true
language: en
---

PhD, senior researcher in MIPT NPM. 

Conducts research in mathematical and computational physics, environmental science, and computational finance, on which he also teaches a master's course.
