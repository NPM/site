---
content_type: member
title: Alexey Khudyakov
id: khudyakov
order: 12
photo: khudyakov.jpg
published: false
language: en
---
INR RAS researcher.

Specialist in rare caonic decay, data collection systems and functional programming.

