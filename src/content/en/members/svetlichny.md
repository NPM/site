---
content_type: member
title: Alexander Svetlichny
id: svetlichny
order: 14
photo: svetlichny.jpeg
published: true
language: en
---

Researcher at INR RAS, PhD and researcher at MIPT, teacher at general physics department at MIPT, deputy head of [the scientific programming master program](/magprog).

Researching Monte-Carlo modeling of relativistic nucleus-nucleus collision under supervision of I. Pshenichnov. Develops new approaches to education.

ORCID: https://orcid.org/0000-0002-2086-7045

ResearchGate: https://www.researchgate.net/profile/Alexandr-Svetlichnyy
