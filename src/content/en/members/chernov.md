---
content_type: member
title: Vasily Chernov
id: chernov
order: 22
photo: chernov.png
published: false
language: en
---
PhD in Physics and Mathematics.

Data acquisition systems specialist. Participates in the Troitsk nu-mass and IAXO experiments.
