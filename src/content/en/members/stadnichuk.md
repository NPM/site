---
content_type: member
title: Egor Stadnichuk
id: stadnichuk
photo: stadnichuk.jpg
order: 60
published: true
language: en
---
Laboratory researcher. Postgraduate student at the Higher School of Economics.

Head of Segmented Satellite Detector. The main participant in works on atmospheric physics.
