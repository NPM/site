---
content_type: member
title: Mikhail Zeleny
id: zelenyy
order: 17
photo: zelenyy.jpg
published: true
language: en
---
INR RAS postgraduate student. MIPT teacher.

The head of the working group on modeling physical processes.