---
content_type: member
title: Sergey Nikolaevich Zhabin
id: zhabin
order: 11
photo: zhabin.jpg
published: false
language: en
---
Candidate of physical and mathematical sciences. MIPT teacher.