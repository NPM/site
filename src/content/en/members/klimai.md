---
content_type: member
title: Peter Klimai
id: klimai
order: 2
photo: klimai.jpg
published: true
language: en
---
**Deputy head of the laboratory**

PhD in Physics and Mathematics.

Head of MIPT-NPM work on software for the BM@N experiment. 

Main publications in [INSPIRE](https://inspirehep.net/literature?sort=mostrecent&size=25&page=1&q=klimai&ui-citation-summary=true)
