---
content_type: member
title: Almaz Fazliakhmetov
id: almaz
order: 20
photo: almaz.jpg
published: true
language: en
---
Postgraduate student.

Team leader on [muon monitor](/projects/physics#mounMonitor).