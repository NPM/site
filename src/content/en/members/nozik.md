---
content_type: member
title: Alexander Nozik
id: nozik
order: 2
photo: nozik.png
page_link: /about/nozik
published: false
language: en
---
**Deputy team leader.**
 
Candidate of physical and mathematical sciences. Senior researcher at INR RAS (Sector of mathematical support of experiments), Assistant of department of general physics, MIPT.

Specialist in data analysis in physical experiment. Member of the experiment "Troitsk nu-mass" for the direct measurement of the neutrino mass. [ResearchGate profile](https://www.researchgate.net/profile/Alexander_Nozik)

Head of the software group in [JetBrains Research](https://research.jetbrains.org/researchers/altavir).

Secretary of the council [Society of Scientists](http://onr-russia.ru/)

**Scientific interests:** Mathematical statistics, scientific software, neutrino mass.

e-mail: [nozik.aa@mipt.ru](mailto:nozik.aa@mipt.ru)
