---
content_type: member
title: Daria Zemlianskaya
id: zemlianskaya
order: 520
photo: zemlianskaya.jpg
published: false
language: en
---

MIPT student, atmospheric physics group. Engaged in modeling processes in atmospheric physics.