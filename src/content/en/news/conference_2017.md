---
content_type: post
title: Youth conference 2017
date: 2017-01-31
published: true
slug: /conf_2017
language: en
---

<img className="img-fluid" src="/images/index/conference_2017_header.png" className="img-responsive" alt="conference slider">

Registration for the III youth particle physics and cosmology conference is now open. The conferenc will be held on April 27 and 28. 

Registration and additional information are available at [conference site](http://npm.mipt.ru/conf).
 
Information flyer is available [here](/files/conference_2017_invitation.pdf).