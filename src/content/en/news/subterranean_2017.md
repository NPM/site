---
content_type: post
title: Low-background experiments in the nuclear, particle physics and astrophysics
date: 2017-02-06
published: true
slug: /subterranean
language: en
---
<p className="lead">
Semester elective course
</p>

L.V. Inzhechik reads lectures on Mondays at 18:30 in aud. 517 of Main Building. The first lecture &mdash; 20th February 2017.

More information on the [course page](/pages/subterranean).