---
content_type: post
title: Fall semester 2018
date: 2018-09-07
published: true
slug: /autumn_2018
language: en
---
Congratulations to all on the start of the new academic year!

Two our traditional courses will take place this semester:

<hr>

## [Non-accelerationf experimants in particle physics and astrolhysics](/pages/subterranean)
**The 1st lecture &mdash; 19th September 2018**

Lecturer: Lev Inzhechik, PhD, a member of the international collaborations GERDA, LEGEND, EMMA and Mu-MONITOR.

**Date, time and place: every Wednesday, at 17.05, in 517-А MB (teaching room of the department of general physics).**

The course is devoted to the problems of neutrino physics, astrophysics and nuclear physics. GERDA, EMMA, Mu-MONITOR, TROITSK NU-MASS and KATRIN experiments will be discussed in detail.

<hr> 

## [Statistical methods in experimantal physics](/pages/stat-methods)
**The 1st lecture &mdash; 26th September 2018 г**

Lecturer: Alexander Nozik, PhD

**Date, time and place: every Wednesday, at 17.05, in 403 LB.**

The course details the use of statistical methods in planning and processing the results of a physical experiment. The emphasis will be placed not on theory, but on practical application. In particular, computer methods for data analysis will be analyzed.
<hr>

## Scientific work

The group continues scientific work jointly with leading scientific organizations both in Russia and abroad.

We also announce recruitment for second and third year students for scientific work in several areas.
Student vacancy announcements are posted on [document server](http://npm.mipt.ru/confluence/pages/viewpage.action?pageId=16023618).

We invite everyone to the scientific meetings that will be held **every Wednesday, at 12.20, in 403 LB**. 

If you have any questions please write to [group email or telegram](/about).

<hr>

[Announcement of lectures and scientific work 2018](/files/npm-2018.pdf)