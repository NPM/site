---
content_type: post
title: Programming seminar
date: 2019-06-10
published: true
slug: /prog_seminar
language: en
---
Laboratory jointly with JINR and with the support of [JetBrains Research](https://research.jetbrains.org/groups/npm) 
launches a series of programming workshops in experimental physics (and not only). Information can be found [here](projects/../../../projects/software#prog-seminar) and [here](http://npm.mipt.ru/confluence/pages/viewpage.action?pageId=33128452). If you would like to participate, please help determine the dates of the first workshop ( [doodle](https://doodle.com/poll/gircdvu5kya77twf) ). Also, if you have something to tell about, write to mail or telegram.