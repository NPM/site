---
content_type: post
title: The official version of the site has been launched.
date: 2016-09-25
published: true
slug: /develop
language: en
---

The official version of the group site has been launched. Some sections are still under development.