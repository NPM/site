---
content_type: post
title: Updated projects and new vacancies
date: 2020-10-19
published: true
slug: /physics_update_october_2020
language: en
---

The section with [particle physics projects](/projects/physics/) on the site is updated. Added sections on atmospheric physics and solar particle detector (text for both sections was prepared by Egor).

Published [article on satellite detector](https://arxiv.org/abs/2005.02620). An article on accounting for the number of [runaway electrons in the Gurevich model](https://arxiv.org/abs/2008.05929) was accepted for publication.

In addition, two vacancies were opened. One vacancy in nuclear physics (more details [here](https://npm.mipt.ru/confluence/pages/viewpage.action?pageId=57573638)). 
We are looking for a third year or older student interested in this topic. 

Second vacancy is in data collection systems field. We are looking for a person who would undertake to master this topic (mainly software + work with protocols of the hardware and transport level) with subsequent work at DESY (Hamburg) on the IAXO experiment.

Contacts [are, as usual, here](/about/).