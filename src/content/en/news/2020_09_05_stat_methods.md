---
content_type: post
title: Statistical Methods 2020
date: 2020-09-06
published: true
slug: /stat_methods_2020
language: en
---

In the fall of 2020, we continue our now traditional course [Statistical Methods in Experimental Physics](pages/stat-methods). As in the previous year, the course is combined with the basic course "Introduction to Data Analysis", read within the framework of "Physics Horizons" for the second year students from the basic department of the INR RAS "Fundamental Interactions and Cosmology".

This year, the course will be partly remote and with minor format changes. Lectures will be more clearly separated from seminars and will be recorded. The workshops will include significantly more demonstrations of the specific program code used for data analysis. In particular, there will be several sessions of the so-called live-coding. Also, due to the transfer of lectures online (and the help of assistants), we plan to supplement the course program with separate lessons devoted to modern aspects of working with data, such as Monte Carlo methods and Bayesian methods.

The course will be announced online on Wednesday, September 9 at 5:05 pm &nbsp;[https://meet.google.com/fqh-izkt-rfu](https://meet.google.com/fqh-izkt-rfu).
To distribute relevant information on the course, as well as for questions and discussion, a Telegram group was created: [https://t.me/mipt_statmethods](https://t.me/mipt_statmethods).

Additional course materials will be available [here](https://npm.mipt.ru/confluence/pages/viewpage.action?pageId=56655879).

The course is conducted with [JetBrains Research informational support](https://research.jetbrains.org/ru/groups/npm/courses/3).