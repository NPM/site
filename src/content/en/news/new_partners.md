---
content_type: post
title: New partners
date: 2016-11-21
published: true
slug: /partners
language: en
---
On November 19, another working group meeting was held. Two new cooperation agreements were discussed during the meeting.

Started cooperation with MTL. As a part of this cooperation, joint research and development of X-ray panels and the preparation of students are planned.

Also as a part of cooperation with the SRI of RSA a working group has been created to model electric discharges in the upper atmosphere.

updated ["Partners"](/partners) section.