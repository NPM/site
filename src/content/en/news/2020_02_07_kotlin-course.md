---
content_type: post
title: Scientific programming on Kotlin — 2020
date: 2020-02-07
published: true
slug: /kotlin_2020
language: en
---

Registration for the course "Introduction to Scientific Programming in the Kotlin Language" is open. This year the course is officially supported by [JetBrains](https://jetbrains.com) and [JetBrains Research](https://research.jetbrains.org/groups/npm).

Course page is available [here](/pages/kotlin). To participate in the course, fill the [form](https://docs.google.com/forms/d/e/1FAIpQLSeNZT8B90pT6fM9oABHFbrtv6pKfoYKfO-ANAjLlgWynMnh_g/viewform).