---
content_type: post
title: Elementary Particle Physics and Cosmology 2019
date: 2019-02-26
published: true
slug: /conference
language: en
---
Registration for the youth conference "Elementary particle physics and cosmology 2019" is now open. The conference is dedicated to particle physics, cosmology and wide range of related topics like mathematical physics, astrophysics, nuclear medicine, etc. 
The conference is held for the eighth time and despite the low number of reports it has established itself as one of the representative Russian-speaking youth conferences on this topic.

The conference will be held on April 11 and 12 in the Moscow building of the Moscow Institute of Physics and Technology at Klimentovsky per., bld.1, p.1. Participation in the conference is free, but the number of reports is limited.

Conference proceedings will be published in electronic form. Selected papers will be recommended for publication in the journal "MIPT Proceedings".

Links:

* [Conference site](https://belle.lebedev.ru/conf_mipt_2019/)

* [Registration]( https://belle.lebedev.ru/conf_mipt_2019/?page_id=98)

* [Poster](http://belle.lebedev.ru/conf_mipt_2019/wp-content/uploads/2019/01/%D0%A4%D0%B8%D0%B7%D0%B8%D0%BA%D0%B0-%D1%8D%D0%BB%D0%B5%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%80%D0%BD%D1%8B%D1%85-%D1%87%D0%B0%D1%81%D1%82%D0%B8%D1%86-2019.pdf)

We kindly ask you to widely disseminate information about the conference, and register as soon as possible
