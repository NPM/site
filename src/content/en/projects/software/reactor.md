---
content_type: project
project_type: software
id: reactor
shortTitle: RL-TGE modeling
title: Macro modeling for a reactor model in atmospheric physics
order: 10
published: true
language: en
---

Macro simulation for the reactor model in the physics of electron avalanches in the atmosphere in the Kotlin language.

[Repository](https://bitbucket.org/mipt-npm/skysim)
