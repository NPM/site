---
content_type: project
project_type: software
id: dataforge
shortTitle: KMath
title: Experimental Kotlin math library
order: 2
published: true
language: en
---

An experimental Kotlin library for mathematical operations, built on the principle of context-oriented programming using mathematical abstractions.

[Repository and documentation](https://github.com/altavir/kmath)