---
content_type: project
project_type: software
id: dataforge
shortTitle: DataForge
title: DataForge, an automated data processing system
order: 1
published: true
language: en
---

[DataForge](/dataforge) is a modern platform for collecting and analyzing data, designed to automate data processing in physical experiments and not only.