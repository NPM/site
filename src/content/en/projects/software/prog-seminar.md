---
content_type: project
project_type: software
id: prog-seminar
shortTitle: Programming workshop
title: Programming in experimental physics
order: 300
published: true
language: en
---

It is no secret that conducting and analyzing data from a modern physical experiment at almost every stage is connected with the work on computers, with the development and use of various software. In such a situation, the approaches used in the development of programs and the quality of these programs become very significant.

The Laboratory of Nuclear Physics Experimental Methods at MIPT, together with JINR, is organizing, with the support of JetBrains Research, a regular seminar on the development, support and use of software in experimental physics.

Workshop topics:

* Software problems in physical experiments and ways to solve them.

* The use of modern IT methods in scientific software.

* Reviews of specific science packages.

* Development and discussion of new tools and methods in scientific programming.

Participation is free. A video broadcast (TBD) will be conducted.

Applications for participation with the report as well as questions can be sent to npm@mipt.ru.

Some issues related to the seminar can be discussed in the [telegram channel of the laboratory](https://t.me/mipt_npm).

Actual information available [here](http://npm.mipt.ru/confluence/pages/viewpage.action?pageId=33128452).