---
content_type: project
project_type: software
id: plotly
shortTitle: Plotly.kt
title: Plotly.kt wrapper library for Kotlin Multiplatform
order: 5
published: true
language: en
------------

[Plotly.kt](https://github.com/mipt-npm/plotly.kt) library wraps popular [Plotly](https://plotly.com/javascript/) frontend library and allows access to it from Kotlin Multiplatform (both from front-end and back-end) as well as Kotlin kernel for IPython/Jupyter support and other nice things.

More detailed description is available in the [project repository](https://github.com/mipt-npm/plotly.kt) and on the [special page](/files/plotly.html) prepared by Ekaterina Samorodova.