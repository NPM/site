---
content_type: project
project_type: software
id: muon-sim
shortTitle: Modeling Muon Monitor
title: Muon Monitor experiment data analysis model
order: 9
published: true
language: en
---

Modeling and graphical visualization of muon registration in the Muon Monitor experiment written in the Kotlin language.

[Repository and launch instructions](https://bitbucket.org/mipt-npm/muon-sim)
