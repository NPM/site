---
content_type: project
project_type: education
id: labs
shortTitle: Laboratory works
title: Laboratory work at the department of general physics
courseName: biref
order: 10
published: true
language: en
---

The group is involved in improving laboratory work at the department of general physics, MIPT.

### Birefringence

A software package and an additional description for processing data in laboratory work on birefringence in optics course (IV semester of general physics).

The web application is available [here](https://npm.mipt.ru/apps/biref).

Details available at the [project page](/pages/biref).

The code is [on the github](https://github.com/mipt-genphys/birefringence).

### Manual for conducting and processing the results of a training experiment

The manual is intended for junior students and describes the conduct, processing and presentation of the results of a typical educational experiment (laboratory work).

**At the moment, the manual is being finalized and will be updated.** 

[PDF version for download](/files/main.pdf).

[On-line version](http://npm.mipt.ru/books/lab-intro/)