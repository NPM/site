---
content_type: project
project_type: education
id: kotlin
shortTitle: Scientific programming
title: Introduction to Kotlin scientific programming
courseName: kotlin
order: 9
published: true
language: en
---
Optional course for everyone.