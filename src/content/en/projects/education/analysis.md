---
content_type: project
project_type: education
id: analysis
shortTitle: Data analysis course
title: Statistical methods in experimental physics
courseName: stat-methods
order: 2
published: true
language: en
---
Optional semester course for 2&ndash;4th year students.