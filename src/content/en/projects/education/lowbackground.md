---
content_type: project
project_type: education
id: lowbackground
shortTitle: Low background experiment course
title: Low background subterranean laboratory experiments course
courseName: subterranean
order: 1
published: true
language: en
---

The course provides a brief introduction to particle and nucleus physics. The problems of modern neutrino physics are considered: rest mass, oscillations, lepton number violation, sterile neutrinos. The latest research methods for neutrinos of various origins are the detection of reactor, solar, atmospheric, accelerator, galactic, and geo-neutrinos. An overview of the underground, underwater and under-ice laboratories of the world is given, where low-background experiments in particle physics and astrophysics are conducted.