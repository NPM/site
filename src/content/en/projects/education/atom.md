---
content_type: project
project_type: education
id: atom
shortTitle: Lecture series on the history of the atomic project
title: History of atomic project
courseName: atom
order: 3
published: true
language: en
---

The history of the atomic project of the USSR first-hand.