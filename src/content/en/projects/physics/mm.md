---
content_type: project
project_type: physics
id: mounMonitor
shortTitle: Muon monitor
title: Muon monitor for subterranean low-background experiments
order: 2
published: true
language: en
---
**Location:** Spain, Canfranc-Estación, Canfranca Underground Laboratory (LSC Laboratorio Subterráneo de Canfranc)

<img src="/images/projects/physics/map.png" alt="map"/>

**Objective:** Registration of high-energy cosmic muons in Underground LSC laboratory conditions.

**Installation scheme:** basic detecting elements of the monitor are SC16 muon scintillation detectors, each of which in in turn consists of 16 single scintillation detectors SC1 (“pixels”) and internal electronics.

The data acquisition system (DAQ) includes signal processing units about the time (TimeBoard) and the coordinate (HodoscopeBoard) of the triggered pixels; low voltage power supplies for detectors; VME units and computer for final output of source files; Trigger Unit for muon selection real time events.

DAQ system:

Electronics

-   Power supply for the sc16 detectors

-   Hodoscope board (pattern info)

-   Time master board (timing info)

-   7 power supplies for these boards

-   VME modules

-   VME Pattern unit V1495

-   Computer with data acquisition program

-   Trigger Unit (real time selection)

Scintillation Detectors SC16 Scintillation Monitor Systems
grouped in three layers. The upper and lower layers consist of 9 detectors
SC16 with the total number of SC1 scintillators in each layer equal to 9\* 16
= 144. The middle layer consists of 4 SC16 detectors, i.e., 64 scintillators
SC1.

Middle and top layer detectors rest on a wooden frame. Frame leans on the bottom layer. On top of the assembly of three layers is covered with lead screen.

<img src="/images/projects/physics/setup.png" alt="setup"/>

Installation scheme