---
content_type: project
project_type: physics
id: gerda
shortTitle: GERDA
title: International experiment GERDA
order: 1
published: true
language: en
---

GERDA (GERmanium Detector Array) experiment, marketed at the Gran Sasso National Laboratory in Italy, designed to search for Ge-76 double neutrino-free decay. GERDA uses detectors from Germany enriched in the Ge-76 isotope. The detectors are immersed in liquid argon, which cools them to a working temperature (87 K) and, at the same time, serves as additional protection against background radiation. The experiment is carried out in several stages or phases. At the moment, the second phase of the experiment is over and a third is planned.

<img src="/images/projects/physics/GERDA.jpg" alt="GERDA"/>

However, with an increase in the accuracy of the experiment in the following phases, one of the sources of background events may be the interaction of 76Ge with neutrinos from the sun. Members of the group deal with this problem in collaboration with specialists from the Kurchatov Institute. This study is based on preliminary calculations of group leader Inzhechik Lev Vladislavovich (he is a member of the GERDA collaboration).