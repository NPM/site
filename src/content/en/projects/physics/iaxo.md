---
content_type: project
project_type: physics
id: iaxo
shortTitle: IAXO
title: International collaboration IAXO
order: 6
published: false
language: en
---

**IAXO** (International Axion Observatory) is a new generation of the axion helioscope, whose main task is the detection of axions (or other elementary particles), in a large number emitted by the solar core.

**Axions** are hypothetical particles proposed in an extension of the standard model of particle physics. Their existence has not been experimentally proven, but there are serious theoretical reasons to suspect this. They are also associated with the problem of dark matter.

The helioscope uses a powerful magnetic field to convert axions into photons. A toroidal superconducting magnet with a length of 20m with eight coils and eight holes with a diameter of 60 cm located between the coils is used. This magnet will be placed on a moving structure, very similar to the usual telescopic, to direct the magnet to the Sun. At the end of the magnet holes, a specially designed X-ray optics focuses the proposed axion photons in small areas (0.2cm $^2$) at a focal length of about 5 meters. Each of the focal spots will be displayed using Ultra-low Micromegas background x-ray detectors.

**The laboratory's task** is development of software and a slow control system.
<img src="/images/projects/physics/iaxo.png" alt="IAXO"/>