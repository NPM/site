---
content_type: project
project_type: physics
id: numass
shortTitle: Troitsk nu-mass
title: Neutrino mass search facility Troitsk nu-mass
order: 3
published: true
language: en
---

<img src="/images/projects/physics/spectrometer900.jpg" alt="spectrometer"/>

The Troitsk nu-mass facility is one of the few world-class experiments in Russia in the field of particle physics. The purpose of the experiment is to search for masses of both active and sterile neutrinos. The results obtained at the installation are currently the best in the world.