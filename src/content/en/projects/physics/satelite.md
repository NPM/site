---
content_type: project
project_type: physics
id: satelite
shortTitle: Satellite detector
title: Satellite detector of solar radiation
order: 7
published: true
language: en
---

Researchers from the Laboratory of Nuclear Physics Methods have developed a prototype detector for solar cosmic ray spectroscopy. The customer for the work was the Space Research Institute of the Russian Academy of Sciences. A prototype detector was assembled and tested at the Institute for Nuclear Research, Russian Academy of Sciences. Research results are published [in JINST](https://iopscience.iop.org/article/10.1088/1748-0221/15/09/T09006).

Structurally, the detector is a scintillation cylinder [segmented into several washers](#sat_detector). The energy release of the particle passing through the detector is removed from each washer separately. This design makes it possible to reconstruct the loss curve of registered particles, which increases the energy resolution of the detector in comparison with classical calorimeters. Moreover, due to segmentation, the detector is able to operate in the so-called integral mode. In this operating mode, the detector records the total loss curve from several incident particles during the exposure. The laboratory staff showed that using the Turchin regularization method, it is possible to reconstruct with good accuracy the spectrum of particles registered by the device from their total loss curve. Integral mode allows the detector to operate when the frequency of particles hitting the detector exceeds the readout speed of the electronics. This gives the device an advantage over existing solar cosmic ray spectrometers.

<figure id="sat_detector">
    <img src="/images/projects/physics/satelite/detector.png" alt="detector"/>
    <figcaption>Figure 1. Device prototype. 1 &mdash; detector body contained from scintillation washers, 2 &mdash; shielded fiber optic, 3 &mdash; bias voltage control and data acquisition boards developed at JINR, 4 &mdash; hull and stand of the prototype (for ground research).</figcaption>
</figure>


<figure id="sat_reconstruction">
    <img src="/images/projects/physics/satelite/reconstruction.png" alt="reconstruction"/>
    <figcaption>Figure 2. An example of reconstruction of a proton spectrum by a detector in an integrated operating mode. Geant4 simulation data is used.</figcaption>
</figure>


