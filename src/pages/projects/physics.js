import React from "react"
import {graphql} from "gatsby"
import {injectIntl, useIntl} from "gatsby-plugin-react-intl"

import Layout from "../../components/layout"
import Projects from "../../components/templates/projectsTemplate";

const PhysicsPage = ({data}) => {
    const intl = useIntl()
    const lang = intl.locale
    let physicsProjects = "";
    if (lang === "ru") {
        physicsProjects = data.ru_projects.edges;
    } else if (lang === "en") {
        physicsProjects = data.en_projects.edges;
    }
    return (
        <Layout>
            <Projects projects={physicsProjects} project_type="physics"/>
        </Layout>
    )
}

export default injectIntl(PhysicsPage)

export const query = graphql`
    query{
        ru_projects: allMarkdownRemark(filter: {frontmatter: {content_type: {eq: "project"}, project_type: {eq: "physics"} published: {ne: false}, language: {eq: "ru"}}},
            sort: {fields: [frontmatter___order], order: ASC}){
            edges{
                node{
                    html
                    frontmatter{
                        shortTitle
                        title
                        id
                    }
                }
            }}

        en_projects: allMarkdownRemark(filter: {frontmatter: {content_type: {eq: "project"}, project_type: {eq: "physics"} published: {ne: false}, language: {eq: "en"}}},
            sort: {fields: [frontmatter___order], order: ASC}){
            edges{
                node{
                    html
                    frontmatter{
                        shortTitle
                        title
                        id
                    }
                }
            }}
    }
`