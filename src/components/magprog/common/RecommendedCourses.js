import React, {useState} from "react";
import {Button, Collapse} from "react-bootstrap";

export function RecommendedCourses(props) {
    const [open, setOpen] = useState(false);

    return (
        <>
            <Button
                size="lg"
                className="fit primary"
                onClick={() => setOpen(!open)}
                aria-controls="recommended-courses-collapse-text"
                aria-expanded={open}
            >
                Рекомендованные курсы
            </Button>
            <Collapse in={open} className="pt-3">
                <div id="recommended-courses-collapse-text">
                    {props.children}
                </div>
            </Collapse>
        </>
    );
}