import React from 'react';


export function Partners({data}) {
    return <div className="inner">
        <h2>{data.title}</h2>
        <div className="features">
        {data.content.map(item =>{
            return <section>
                <a href={item.link} target="_blank" rel="noreferrer">
                <img className="icon major" src={item.logo} alt={item.logo}/>
                <h3>{item.title}</h3>
                </a>
            </section>
        })}
        </div>
    </div>
}
