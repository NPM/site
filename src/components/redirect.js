import React from "react"
import {injectIntl} from "react-intl"
import Seo from "./seo"

const Redirect = ({intl}) => {
    return <Seo title={`${intl.formatMessage({id: "title"})}`}/>
}

export default injectIntl(Redirect)
