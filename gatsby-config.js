module.exports = {
    siteMetadata: {
        title: `MIPT-NPM laboratory`,
        description: `Nuclear Physics Methods Laboratory`,
    },
    plugins: [
        {
            resolve: 'gatsby-source-filesystem',
            options: {
                path: `${__dirname}/src/images`,
                name: 'images',
            },
        },
        {
            resolve: `gatsby-source-filesystem`,
            options: {
                name: `content`,
                path: `${__dirname}/src/content`,
            },
        },
        `gatsby-plugin-sharp`,
        `gatsby-transformer-sharp`,
        {
            resolve: `gatsby-transformer-remark`,
            options: {
                plugins: [
                    'gatsby-remark-katex',
                    {
                        resolve: 'gatsby-remark-images',
                        options: {
                            maxWidth: 100
                        }
                    },
                ],
            },
        },
        `gatsby-plugin-catch-links`,
        `gatsby-plugin-react-helmet`,
        {
            resolve: `gatsby-plugin-manifest`,
            options: {
                name: `NPM group`,
                short_name: `npm`,
                lang: `ru`,
                start_url: `/`,
                // start_url: `/new`,
                display: `minimal-ui`,
                icon: `src/images/icon.png`, // This path is relative to the root of the site.
            },
        },
        {
            resolve: `gatsby-plugin-react-intl`,
            options: {
                path: `${__dirname}/src/intl`,
                languages: [`ru`, `en`],
                defaultLanguage: `ru`,

                redirect: true,
                redirectComponent: require.resolve(`./src/components/redirect.js`),
            },
        },
        'gatsby-plugin-sass',
    ],
}