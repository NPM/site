const { Client } = require('node-scp')

let remotePath = "/var/www/html"

let args = process.argv.slice(2);

async function deploy() {
    try {
        console.log("Starting scp upload")
        const client = await Client({
            host: 'npm.mipt.ru',
            port: 22,
            username: args[0],
            password: args[1],
        })
        await client.emptyDir(remotePath)
        await client.uploadDir('./public', remotePath)
        client.close() // remember to close connection after you finish
    } catch (e) {
        console.log(e)
    }
}

deploy().then(r => console.log("Deploy is finished"))
