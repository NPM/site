const path = require('path')
const ncp = require('ncp').ncp;

const filesSource = "./src/files"
const imagesSource = "./src/images"
const webpack = require('webpack')


exports.onPreBootstrap = () => {
    ncp(filesSource, "./static/files", function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('Files copied');
    });
    ncp(imagesSource, "./static/images", function (err) {
        if (err) {
            return console.error(err);
        }
        console.log('Images copied');
    });
}

exports.createPages = async ({actions, graphql}) => {
    const {createPage} = actions;

    // language=GraphQL
    const result = await graphql(`
        {

            news: allMarkdownRemark(
                filter: {frontmatter: {content_type: {eq: "post"}, published: {ne: false}}},
                sort: {fields: [frontmatter___date], order: DESC}
            ) {
                edges{
                    node{
                        frontmatter{
                            title
                            slug
                            date(formatString: "DD.MM.YYYY")
                        }
                        html
                    }
                }
            }

            coursePages: allMarkdownRemark(
                filter: {frontmatter: {content_type: {eq: "course"}, published: {ne: false}}}
            ){
                edges{
                    node{
                        html
                        frontmatter{
                            path
                            slug
                        }
                    }
                }
            }

            personalPages: allMarkdownRemark(
                filter: {frontmatter: {content_type: {eq: "personal_page"}, published: {ne: false}}}
            ){
                edges{
                    node{
                        html
                        frontmatter{
                            path
                            id
                            language
                        }
                    }
                }
            }
        }
    `)

    if (result.errors) {
        throw result.errors;
    }

    // Render courses
    const courseTemplate = path.resolve(`./src/components/templates/courseTemplate.js`);
    const courses = result.data.coursePages.edges;

    courses.forEach(({node}) => {
        createPage({
            path: node.frontmatter.path,
            component: courseTemplate,
        })
    });

    //Render personal pages
    const personalPageTemplate = path.resolve(`./src/components/templates/personalPageTemplate.js`);
    const personalPages = result.data.personalPages.edges;

    personalPages.forEach(({node}) => {
        createPage({
            path: node.frontmatter.path,
            component: personalPageTemplate,
            context:{
                id: node.frontmatter.id
            }
        })
    });

    // Render news page

    const newsTemplate = path.resolve(`./src/components/templates/news.js`);
    const posts = result.data.news.edges;

    const postsPerPage = 5;
    const numberOfNewsItems = posts.length;
    const numberOfNewsPages = Math.ceil(numberOfNewsItems / postsPerPage);


    for (let pageIndex = 0; pageIndex < numberOfNewsPages; pageIndex++) {
        const pageNumber = pageIndex + 1;
        const path = pageIndex === 0 ? '/news' : `/news/${pageNumber}`;
        const skip = pageIndex * postsPerPage;

        function getPreviousPageLink() {
            if (!pageIndex) return null
            if (pageIndex === 1) return '/news'
            return `/news/${pageIndex}`
        }

        function getNextPageLink() {
            if (pageNumber < numberOfNewsPages) return `news/${pageNumber + 1}`
            return null
        }

        createPage({
            path,
            component: newsTemplate,
            context: {
                limit: postsPerPage,
                skip,
                next: getNextPageLink(),
                prev: getPreviousPageLink(),
            },
        })
    }
}